import router from '@/routers/router.js';
import Home from '@/views/normal-home/normal-home.vue';

const userLevel = 'normal';
const normalRouterMap = [
  {
    path: '/',
    name: 'home',
    component: Home
  }
];
const vipRouterMap = [
  {
    path: '/',
    name: 'home',
    component: Home
  }
];
router.beforeEach((to, from, next) => {
  if (userLevel === 'normal') {
    router.addRoutes(normalRouterMap);
    next();
  } else {
    // router.addRouters({});
    router.addRoutes(vipRouterMap);
    next();
  }
});
