import Vue from 'vue';
import Router from 'vue-router';
import normalHome from '@/views/normal-home/normal-home.vue';
import vipHome from '@/views/vip-home/vip-home.vue';

Vue.use(Router);

// todo 404 页面
// todo 断网页面

const userLevel = 'vip';

const normalRouterMap = [
  {
    path: '/',
    redirect: '/normal-home'
  },
  {
    path: '/normal-home',
    component: normalHome
  }
];

const vipRouterMap = [
  {
    path: '/',
    redirect: '/vip-home'
  },
  {
    path: '/vip-home',
    component: vipHome
  }
];

let routerMap = [];

if (userLevel === 'normal') {
  routerMap = normalRouterMap;
} else if (userLevel === 'vip') {
  routerMap = vipRouterMap;
}

const createRouter = () =>
  new Router({
    routes: routerMap
  });
const router = createRouter();

export default router;
