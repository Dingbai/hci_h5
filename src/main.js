import Vue from 'vue';
import App from './App.vue';
import 'amfe-flexible';
import router from './routers/router';
import store from './stores/store';

import 'vant/lib/icon/local.css';
import '@/assets/styles/reset.less';
import '@/assets/iconfont/iconfont.css';

// import '@/routers/main.js';

import { Icon, Button } from 'vant';

Vue.use(Icon).use(Button);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
